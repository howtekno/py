# Importing Need Libraries
import json
import urllib.request
import socket
import urllib.parse
from html.parser import HTMLParser
import requests
import pandas as pd
import datetime
from bs4 import BeautifulSoup
import re
from urllib.request import Request, urlopen, ssl, socket
from urllib.error import URLError, HTTPError
import time
import cssutils
from urllib.request import urlopen, Request
import logging
cssutils.log.setLevel(logging.CRITICAL)
from selenium import webdriver
import numpy as np
from PIL import Image
import tldextract
import warnings

warnings.filterwarnings("ignore")
#
#
#
#
#
#

def palette(img):
    """
    Return palette in descending order of frequency
    """
    arr = np.asarray(img)
    palette, index = np.unique(asvoid(arr).ravel(), return_inverse=True)
    palette = palette.view(arr.dtype).reshape(-1, arr.shape[-1])
    count = np.bincount(index)
    order = np.argsort(count)
    return palette[order[::-1]]

def asvoid(arr):
    """View the array as dtype np.void (bytes)
    This collapses ND-arrays to 1D-arrays, so you can perform 1D operations on them.
    http://stackoverflow.com/a/16216866/190597 (Jaime)
    http://stackoverflow.com/a/16840350/190597 (Jaime)
    Warning:
    >>> asvoid([-0.]) == asvoid([0.])
    array([False], dtype=bool)
    """
    arr = np.ascontiguousarray(arr)
    return arr.view(np.dtype((np.void, arr.dtype.itemsize * arr.shape[-1])))


def savePrint(imageFile,url):
    driver = webdriver.Chrome('./chromedriver')
    driver.get(url)    
    driver.get_screenshot_as_file(imageFile)

# Import our Phishing Sites Data
ds = pd.read_csv('./f.csv')

# i is used to stop the for loop for debug
i=0

# done is int to measure how many sites were successfully fetched
done=0

# Int for the # of starting tags and endtags
number_of_starttags = 0
number_of_endtags = 0

# CSSS selectors to in page style and linked style
selectors={}
selectorsRemote={}

# For loop to loop through the data to collect more data

# For every row in ds


# Create needed column
ds['ip']=""
ds['country']=""
ds['city']=""
ds['isp']=""
ds['URLlen']=0
ds['URLage']=-1
ds['URLrank']=0
ds['uiqURLS']=0
ds['SSLstatus']=-1
ds['Favicon']=-1
ds['inlineCSS']=0
ds['linkedCSS']=0
ds['tld']=''
ds['isSubdomain']=-1
ds['c1']=''
ds['c2']=''
ds['c3']=''
ds['/']=0
ds['@']=0
ds['.']=0
ds['-']=0


for index, row in ds.iterrows():
    start = time.time()

    
    # Create Class and override it to parse the web pages
    class MyHTMLParser(HTMLParser):
        
        # Function for handle starting tags
        def handle_starttag(self, tag, attrs):
            
            # The number of starting tags
            global number_of_starttags
            
            # Check if tag is a column in ds if does not exist create it 
            if tag not in ds:
                # init value is 0
                ds[tag]=0
                # get the column index
                k = ds.columns.get_loc(tag)
                # increase the number by 1
                ds.iloc[index,k] +=1
            # If the tad exist 
            else :
                # Get the index
                k = ds.columns.get_loc(tag)
                # Increase by 1
                ds.iloc[index,k] +=1
            # Increase the starting tags by 1
            number_of_starttags += 1

        def handle_endtag(self, tag):
            global number_of_endtags
            number_of_endtags += 1


    # instantiate the parser and fed it some HTML
    #vgm_url = 'https://www.tokopedia.com/'
    
    # Fetch first row url
    vgm_url = ds.iloc[index,0]
    #vgm_url= 'https://' + vgm_url
                    
        #
        #
        #
        #
        #
            
    # Try and catch for offline websites
    try:
        # Prepare and clean the url
        # vgm_url With the Https url.netloc cleaned up url
        url = urllib.parse.urlparse(vgm_url)
        IP_TO_SEARCH    = socket.gethostbyname(url.netloc)

        #
        #
        #
        #
        #
        print('Starting..........')
        print(vgm_url)
        print('Getting website color')
        imageFile = './tmp.png'
        savePrint(imageFile,vgm_url)
        img = Image.open(imageFile, 'r').convert('RGB')
        print('-------------')
        print(palette(img)[0:1])
        print(palette(img)[1:2])
        print(palette(img)[2:3])
        
        # Get id for domColor
        k = ds.columns.get_loc('c1')
        # Store the len of css selectors in the dataframe
        temp=palette(img)[0:1]
        temp2= "#{:02x}{:02x}{:02x}".format(temp[0][0],temp[0][1],temp[0][2])
        print(temp2)
        ds.iloc[index,k] = temp2
        # Get id for domColor
        k = ds.columns.get_loc('c2')
        # Store the len of css selectors in the dataframe
        temp=palette(img)[1:2]
        temp2= "#{:02x}{:02x}{:02x}".format(temp[0][0],temp[0][1],temp[0][2])
        ds.iloc[index,k] = temp2
        # Get id for domColor
        k = ds.columns.get_loc('c3')
        # Store the len of css selectors in the dataframe
        temp=palette(img)[2:3]
        temp2= "#{:02x}{:02x}{:02x}".format(temp[0][0],temp[0][1],temp[0][2])
        ds.iloc[index,k] = temp2

        #
        #
        #
        #
        #
        
        # Getting URL Details
        print('Check the URL TLD')
         # Get id for domColor
        k = ds.columns.get_loc('tld')
        # Store the len of css selectors in the dataframe
        ds.iloc[index,k] = tldextract.extract(vgm_url).suffix
        if tldextract.extract(vgm_url).subdomain:
            k = ds.columns.get_loc('isSubdomain')
            # Store the len of css selectors in the dataframe
            ds.iloc[index,k] = 1
        else:
            k = ds.columns.get_loc('isSubdomain')
            # Store the len of css selectors in the dataframe
            ds.iloc[index,k] = 0

        #
        #
        #
        #
        #
        
        # Request the page for to parse
        try:
            print('Parsing CSS')
            cssurl = vgm_url
            print('Req')
            html = urlopen(Request(cssurl, 
                    data=None, 
                    headers={
                        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36'
                    },timeout=10)).read() # request the initial page
            # Prase it using BeautifulSoup
            print('Soup')
            soup = BeautifulSoup(html, 'html.parser')
            # Loop through the page search for style tag
            print('-Inline CSS')
            for styles in soup.select('style'): # get in-page style tags
                # Parse the stlye tag with cssutils lib
                css = cssutils.parseString(styles.encode_contents())
                # Loop through rules of css to save it in dic type
                for rule in css:
                        if rule.type == rule.STYLE_RULE:
                            style = rule.selectorText
                            selectors[style] = {}
                            for item in rule.style:
                                propertyname = item.name
                                value = item.value
                                selectors[style][propertyname] = value
                # Get id for inlineCSS
                k = ds.columns.get_loc('inlineCSS')
                # Store the len of css selectors in the dataframe
                ds.iloc[index,k] = len(selectors)

            # Loop through the page for link tag with type of text/css or rel of stylesheet
            print('- Linked CSS')
            for link in soup.find_all('link', type='text/css') or soup.find_all('link', rel='stylesheet'): # get links to external style sheets
                address = link['href'] # the address of the stylesheet
                if address.startswith('/'): # if the address starts with / it is a relative link
                    # Turn it into accessible link
                    address = cssurl + address
                # Make a request to download the stylesheet from the address
                css = urlopen(Request(address, headers={'User-Agent': 'Mozilla/5.0'})).read() 
                # Parse the stlye tag with cssutils lib
                cssRemote = cssutils.parseString(css)
                # Loop through rules of css to save it in dic type
                for rule in cssRemote:
                        if rule.type == rule.STYLE_RULE:
                            style = rule.selectorText
                            selectorsRemote[style] = {}
                            for item in rule.style:
                                propertyname = item.name
                                value = item.value
                                selectorsRemote[style][propertyname] = value
                # Get id for linkedCSS
                k = ds.columns.get_loc('linkedCSS')
                # Store the len of css selectors in the dataframe
                ds.iloc[index,k] = len(selectorsRemote)
        except Exception as e:
                print('CSS Parsing Erorr')

        #
        #
        #
        #
        #
        
        # Check if Favicon exist
        print('Getting Favicon')
        try:
            html_code = requests.get(vgm_url,verify=False,timeout=10).text
            soup = BeautifulSoup(html_code, features="lxml")
            if soup.find_all('link', attrs={'rel': re.compile("^(shortcut icon|icon)$", re.I)}):
                k = ds.columns.get_loc('Favicon')
                ds.iloc[index,k] = 1
            else:
                k = ds.columns.get_loc('Favicon')
                ds.iloc[index,k] = 0
        except:
            print('Timeout')

        #
        #
        #
        #
        #
        
        # Check SSL Status
        print('Getting SSL')
        hostname = url.netloc
        context = ssl.create_default_context()
        try:
            with socket.create_connection((hostname, '443')) as sock:
                with context.wrap_socket(sock, server_hostname=hostname) as ssock:
                    print(ssock.version())
                    data = json.dumps(ssock.getpeercert())
                    # print(ssock.getpeercert())
            print('SSL Valid')
            k = ds.columns.get_loc('SSLstatus')
            ds.iloc[index,k] = 1
        except:
            print('No SSL Found OR Expired')
            k = ds.columns.get_loc('SSLstatus')
            ds.iloc[index,k] = 0

        #
        #
        #
        #
        #
        
        # Checking Special Chars
        print('Getting Chars')
        k = ds.columns.get_loc('/')
        ds.iloc[index,k] = vgm_url.count('/')
        k = ds.columns.get_loc('@')
        ds.iloc[index,k] = vgm_url.count('@')
        k = ds.columns.get_loc('.')
        ds.iloc[index,k] = vgm_url.count('.')
        k = ds.columns.get_loc('-')
        ds.iloc[index,k] = vgm_url.count('-')
            
        #
        #
        #
        #
        #
        
        # Get number of unique URLS in the page
        print('Get number of unique URLS in the page')
        text = requests.get(vgm_url,verify=False,timeout=10)
       
    
        text = text.text
        url_pattern = r"((http(s)?://)([\w-]+\.)+[\w-]+[.com]+([\w\-\.,@?^=%&amp;:/\+#]*[\w\-\@?^=%&amp;/\+#])?)"
        # Get all matching patterns of url_pattern
        # this will return a list of tuples 
        # where we are only interested in the first item of the tuple
        urls = re.findall(url_pattern, text)
        # using list comprehension to get the first item of the tuple, 
        # and the set function to filter out duplicates
        unique_urls = set([x[0] for x in urls])
        k = ds.columns.get_loc('uiqURLS')
        ds.iloc[index,k] = len(unique_urls)
                
        #
        #
        #
        #
        #
        
        # Getting Alexa Rank
        rank_str =BeautifulSoup(urllib.request.urlopen("https://www.alexa.com/minisiteinfo/" +url.netloc),'html.parser').table.a.get_text()
        k = ds.columns.get_loc('URLrank')
        ds.iloc[index,k] = rank_str
                
        #
        #
        #
        #
        #
        
        # Counting the url char
        print('Couting URL Char')
        k = ds.columns.get_loc('URLlen')
        ds.iloc[index,k] = len(url.netloc)
                
        #
        #
        #
        #
        #
        
        # Query WHOIS to calculate Domain AGE
        print('Calculating Domain Age')
        show = "https://input.payapi.io/v1/api/fraud/domain/age/" + url.netloc
        r = requests.get(show)
        if r.status_code == 200:
            r = requests.get(show)
            data = r.text
            jsonToPython = json.loads(data)
            k = ds.columns.get_loc('URLage')
            ds.iloc[index,k] = round(jsonToPython['result']/365)
                
        #
        #
        #
        #
        #
        
        # Creating request object to GeoLocation API
        print("Requesting DNS Data")
        GEO_IP_API_URL  = 'http://ip-api.com/json/'
        req             = urllib.request.Request(GEO_IP_API_URL+IP_TO_SEARCH)
        # Getting in response JSON
        response        = urllib.request.urlopen(req).read()
        # Loading JSON from text to object
        json_response   = json.loads(response.decode('utf-8'))
        # Same idea get index for the column then add the data to it from the json response
        k = ds.columns.get_loc('ip')
        ds.iloc[index,k] = json_response['query']
        k = ds.columns.get_loc('country')
        ds.iloc[index,k] = json_response['country']
        k = ds.columns.get_loc('city')
        ds.iloc[index,k] = json_response['city']
        k = ds.columns.get_loc('isp')
        ds.iloc[index,k] = json_response['isp']
        
        
        print("Starting Request")
        html_text = requests.get(vgm_url,verify=False, timeout=10).text
        print("Creating Parser")
        parser = MyHTMLParser()
        print("Calling Parser")
        parser.feed(html_text)
        print("Done")
        done += 1
        
    #print(html_text)
    except Exception as e:
        print("An exception occurred")
        print(e)

    
        # end time
    end = time.time()
    #i += 1
    #if i == 1:
    #    break
    # total time taken


        
        
print(done)
ds.to_csv('fakeData.csv')
